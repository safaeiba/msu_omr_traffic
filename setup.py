from setuptools import setup
#from distutils.core import setup

setup(
    name='Omr-Extracter',
    version='0.1dev',
    description='An automated approach toward data extraction from scanned survey forms',
    author='Safaei. B',
    author_email='safaeiba@msu.edu',
    packages=['omr_extracter',],
    license='MIT',
    long_description=open('README.md').read(),
    install_requires=[
        'numpy',
        'pandas',
        'imutils',
        'opencv-python',
        'seaborn',
    ])