{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center> &#9989; Automating Data Extraction from Traffic Survey Forms Using Optical Mark Recognition (OMR) </center>\n",
    "\n",
    "<center>By &#9989; Babak Safaei</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"https://ocr.space/Content/Images/omr-checkbox-10.png\" width=\"40%\">\n",
    "\n",
    "\n",
    "Image from: <a href=\"https://ocr.space/omr\"> OCR.Space </a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Authors\n",
    "\n",
    "&#9989;  Babak Safaei\n",
    "\n",
    "Department of Civil & Environmental Engineering, Michigan State University, East Lansing, MI, USA"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Abstract\n",
    "\n",
    "Providing a safe travel experience for people has always been of great value to governments and transportation agencies. Countries allocate a considerable amount of money each year to improve transportation systems and traffic safety. A significant portion of the mentioned budgets is used in gathering the required data for related studies, mostly done by conducting traffic surveys. One of the so-called surveys happens each year and is focused on collecting data related to driving behavior. The related data can be the reasons affecting the driver's attention toward traffic regulations such as not using cellphones while driving, braking when necessary, following speed limits, and using seatbelts. One of the so called surveys are Safety Belt Surveys (SBS). Survey forms are filled by surveyors who stand by the side of the road and fill the forms according to what they observe. The forms are gathered, and then the data is manually entered into spreadsheets for future use in transportation studies.\n",
    "\n",
    "\n",
    "This software is designed to automate the mentioned process of recording data from SBS forms using Python programming. The gathered forms are scanned, and their information is extracted into spreadsheets by using Optical Mark Recognition (OMR), which is a recognition technology applied on forms such as educational tests to collect data. First, the images are cropped and preprocessed using Gaussian blurring, Canny edge detection, and contrast improvement techniques. Then contours of the image are extracted, and by identifying the outline of the survey, a bird-eye (top-down) view of the document is extracted. Then the software uses the predefined locations on the forms to find questions and answer boxes. The relative darkness of different sections is measured to find the filled boxes. Finally, the data is gathered and saved in a comma-separated file, and required plots are generated upon request."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Statement of Need\n",
    "\n",
    "MSU transportation research group conducts Safety Belt Surveys for collecting data during each Summer. The results of the surveys include hundreds of observations, and according to researchers' experience, the manual approach for extracting data from these surveys is very time-consuming. The mentioned points emphasize the need for software to automate this effort. \n",
    "\n",
    "The final software can be used by the MSU transportation research group or any agencies that may need it.\n",
    "\n",
    "- The sections below include software Installation Instructions, Project Directory Information, Example Usage (How to use the software?), Methodology, Future Works, and Form style."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Installation Instructions\n",
    "\n",
    "First step would be going inside the main project directory and installing the required packages. \n",
    "\n",
    "If you do not already have the project repository, clone it by running the following command in Command line or Terminal.\n",
    "\n",
    "`git clone https://gitlab.msu.edu/safaeiba/msu_omr_traffic.git`\n",
    "\n",
    "\n",
    "Jupyter Notebook (Anaconda 3) and Spyder IDE are recommended for using this software. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are three different approaches to have the required packages installed. Choosing anyone would work (author recommends option 1 or 2). For options 1 and 2, run the following command in Command Line or Terminal when you are inside the project directory to generate a Conda environment, including all of the requirements. You may use option 2, instead of option 1, if you have ` make` commands installed and functioning on your system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Option 1: `conda env create --prefix ./envs --file environment.yml`\n",
    "\n",
    "Option 2: `make init`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, activate the environment."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`conda activate .\\envs`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Option 3: Using PyPi installer file to install the software\n",
    "\n",
    "Run the following code in Command Line or Terminal when you are inside the project directory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`!python setup.py sdist`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, run the code below in Command Line or Terminal when you are inside the project directory to install the software."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`!pip install ./dist/Omr-Extracter-0.1.dev0.tar.gz`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### <em> The step below is not required for using software </em>\n",
    "For building auto-documentation, run the code below. After running the code, you may find the generated document in the docs folder located at the project directory. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Option 1: `pdoc3 --force --html --output-dir ./docs omr_extracter`\n",
    "\n",
    "Option 2: `make doc`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## Project Directory Information\n",
    "\n",
    "`images/`: Contains the scanned images and the output comma separated file file which is result of software\n",
    "\n",
    "`docs/`: Contains the uto documentation of the project code\n",
    "\n",
    "`LICENSE/`: Contains the MIT license file of the project\n",
    "\n",
    "`omr_extracter/`: This is the directory for the project source code and unit test files\n",
    "\n",
    "`REPORTS/`: Contains biweekly reports and examples of computational modeling methods in transportation engineering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Example Usage\n",
    "\n",
    "For using the software, first, follow the installation guidelines provided in the above sections to have the Conda environment with required packages ready. A full example of the whole software and its different steps can be found in `Example` notebook located at the project directory. The `Example` file contains an example of the scanned image and how the software processes it to generate the results. \n",
    "\n",
    "In short, for running the software, after installing the requirements, follow the steps below. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import the software package in a Python environment such as Jupyter Notebook (Anaconda 3), Spyder IDE, Terminal or Command Line)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from omr_extracter import omr_extract"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Run the software and present results. Place the scanned survey images (.jpg format) inside the `~/MSU_OMR_Traffic/images/one`folder. The following code extracts the data from the image located in the mentioned folder.\n",
    "\n",
    "You may store the image you want to use in any location you want, but you need to input its `Path` to the omr_extract.run_omr(`Path`) function. The output will be generated as a file names as `traffic.csv` in the same directory as the image used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "results=omr_extract.run_omr('images/one')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Present the results. The results are stored in numbers format. Explanation of recorded data in the `traffic.csv` file is mentioned below:\n",
    "    \n",
    "- __Vehicle Observablility__ : 0 = Not Observable, 1 = Observable\n",
    "- __Vehicle Use__: 1 = Commercial, 2 = Non-Commercial\n",
    "- __Vehicle Type__: 1 = Passenger Car, 2 = SUV, 3 = Van/Minivan, 4 = Pickup Truck\n",
    "- __Driver Phone Use__: 1 = Handheld (Talking), 2 = Handheld (Typing), 3= Hands-free (E.P.), 4 = Hands-free (No E.P.)\n",
    "- __Driver Seatbelt__: 0 = Unknown, 1 = Belted, 2 = Not Belted\n",
    "- __Driver Age__: 0 = Unknown, 1 = 16-29, 2 = 30-59, 3 = Over 60 years old\n",
    "- __Driver Gender__: 0 = Unknown, 1 = Male, 2 = Female\n",
    "- __Driver Race__: 0 = Unknown, 1 = White, 2 = Black, 3 = Other\n",
    "- __Passenger Presence__: 0 = No, 1 = Yes\n",
    "- __Passenger Seatbelt__: 0 = Unknown, 1 = Belted, 2 = Not Belted\n",
    "- __Passenger Age__: 0 = Unknown, 1 = 0-15, 2 = 16-29, 3 = 30-59, 4 = Over 60 years old\n",
    "- __Passenger Gender__: 0 = Unknown, 1 = Male, 2 = Female\n",
    "- __Passenger Race__: 0 = Unknown, 1 = White, 2 = Black, 3 = Other"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Number</th>\n",
       "      <th>Vehicle Observable</th>\n",
       "      <th>Vehicle Use</th>\n",
       "      <th>Vehicle Type</th>\n",
       "      <th>Driver Phoneuse</th>\n",
       "      <th>Driver Seatbelt</th>\n",
       "      <th>Driver Age</th>\n",
       "      <th>Driver Gender</th>\n",
       "      <th>Driver Race</th>\n",
       "      <th>Passenger Present</th>\n",
       "      <th>Passenger Seatbelt</th>\n",
       "      <th>Passenger Age</th>\n",
       "      <th>Passenger Gender</th>\n",
       "      <th>Passenger Race</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>3</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>3</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>4</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>3</td>\n",
       "      <td>3</td>\n",
       "      <td>1</td>\n",
       "      <td>3</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>3</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>5</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>3</td>\n",
       "      <td>3</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>6</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>7</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>3</td>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>3</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>8</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "      <td>3</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>3</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>9</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>3</td>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>10</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>3</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   Number  Vehicle Observable  Vehicle Use  Vehicle Type  Driver Phoneuse  \\\n",
       "0       1                   1            0             1                1   \n",
       "1       2                   0            1             1                2   \n",
       "2       3                   0            2             2                2   \n",
       "3       4                   0            1             3                3   \n",
       "4       5                   0            1             3                3   \n",
       "5       6                   0            1             2                2   \n",
       "6       7                   0            2             3                2   \n",
       "7       8                   0            1             2                3   \n",
       "8       9                   0            1             3                2   \n",
       "9      10                   0            2             2                2   \n",
       "\n",
       "   Driver Seatbelt  Driver Age  Driver Gender  Driver Race  Passenger Present  \\\n",
       "0                1           1              0            2                  0   \n",
       "1                2           2              0            0                  0   \n",
       "2                1           0              2            3                  0   \n",
       "3                1           3              0            0                  0   \n",
       "4                1           0              1            1                  0   \n",
       "5                2           0              2            2                  0   \n",
       "6                1           0              0            3                  0   \n",
       "7                2           0              2            0                  0   \n",
       "8                1           0              1            2                  0   \n",
       "9                2           0              0            0                  0   \n",
       "\n",
       "   Passenger Seatbelt  Passenger Age  Passenger Gender  Passenger Race  \n",
       "0                   2              0                 0               2  \n",
       "1                   0              2                 1               2  \n",
       "2                   1              3                 1               1  \n",
       "3                   0              2                 0               3  \n",
       "4                   1              0                 1               2  \n",
       "5                   0              2                 0               0  \n",
       "6                   1              2                 1               0  \n",
       "7                   0              3                 0               1  \n",
       "8                   1              2                 1               0  \n",
       "9                   1              3                 0               0  "
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data=results[0]\n",
    "data.head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Methodology\n",
    "\n",
    "The Python packages below were used in this software.\n",
    "\n",
    "- __py-opencv__, __pillow__ and __imutils__: Used for background subtraction, loading images and image processing\n",
    "- __pylint__, __pytest__, __pdoc3__, __autopep8__ and __pydocstring__: Used for improving the coding style and structure of the software\n",
    "- __numpy__ and __pandas__: Used for recording the data in comma-separated-file\n",
    "- __matplotlib__ and __seaborn__: Used for data visualization\n",
    "- __unit testing__: Used for testing the accuracy of the software \n",
    " \n",
    "\n",
    "The steps for building this software are briefly mentioned below.\n",
    "\n",
    "- In the first step, scannable survey form were generated to be used by Python for data extraction. This form was filled using Paint software for testing and using during the project.\n",
    "\n",
    "- The scanned survey images are cropped and preprocessed using Gaussian blurring, Canny edge detection, and contrast improvement techniques.The so called process, removes the noise from the survey image and makes it ready for image recognition. \n",
    "\n",
    "- Contours of the image are extracted, and by identifying the outline of the survey, a bird-eye (top-down) view of the document is extracted. Notably, outline of the survey is the black outer box around the form.\n",
    "\n",
    "- The program uses the predefined locations on the forms to find questions and answer boxes. The relative darkness of different sections are measured to find the filled boxes. The boxes that are darker than other are counted as filled ones by the surveyer.\n",
    "\n",
    "- In the last step, data is gathered and saved in a comma-separated file. Required plots are generated upon request.\n",
    "\n",
    "Notbaly, since I did not have access to actual survey forms and a printer due to university closure, I filled the survey forms using Paint software and tested the software. \n",
    "\n",
    "Overall, the original purpose of this software to automate data extraction from survey forms was totally met. So far, the software works great with minor errors. It is notable that the final software would be helpful for the MSU transportation research group."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Future Works\n",
    "\n",
    "Per future work, I plan to generalize the software and make it more flexible. To be more specific, I am going to improve the code so it can generate a survey form by getting a list of questions and possible answers. Besides, it will be generalized to work on different survey forms. I am also planning to include automated data analysis parts to the software. Finally, it will be able to generate a new form, process it, and extract data, analyze the data according to predefined independent and dependent variables and generate results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Mentions\n",
    "\n",
    "This software will be used by the MSU transportation research group in the future."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Form Outline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./images/Raw_Form/raw_form.jpg\" width=\"70%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# References\n",
    "\n",
    "#### This software can be found on GitLab: <a href=\"https://gitlab.msu.edu/safaeiba/msu_omr_traffic.git\">Omr_extracter</a>\n",
    "\n",
    "__1-__ <a href=\"https://www.pyimagesearch.com/2016/10/03/bubble-sheet-multiple-choice-scanner-and-test-grader-using-omr-python-and-opencv/\">Bubble Sheet Multiple Choice Scanner and Test Grader Using OMR, Python and OpenCV - PyImageSearch.</a>\n",
    "\n",
    "__2-__ <a href=\"https://github.com/rbaron/omr\">GitHub - Rbaron/Omr: Optical Mark Recognition in Python.</a>\n"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
